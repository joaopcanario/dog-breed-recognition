# Dog Breed Recognition

Recognition of One Hundred Dog Breeds Project

## Project dependencies

This project uses Poetry for Python dependency management. The dependency list is available on the `pyproject.toml` file. To Poetry installation, please follow the instructions on the [Poetry's page](https://python-poetry.org/).

## Basic commands

> It is strongly suggested the use of a computer with GPU available but even if it is not possible a computer with good CPU it will be more than enough. An observation about the dev and docker mode: the dev mode makes use of a simple SQLite database to store the principal data. On the other hand, the docker mode, which is based on the Nvidia PyTorch docker image, uses the Postgresql database deployed in the docker container.

- To run on *Dev mode* and execute the project locally, uses the command `make rundev`.
- To to build and run the docker container through the *Docker mode* uses the `make docker` command.
- Run on your terminal `make help` to check all commands available.

Running the dev or docker mode, open the following URL on your preferred browser: [http://0.0.0.0:6500/](http://0.0.0.0:6500/).

## Experiments Folder

We made some data exploration, classification methods, and tests are available in the `experiments` folder. At each stage of this project development, we exploit the tradeoff balance between the accuracy and the required system performance (on CPU or GPU level). Thus, we believe that computes the distance between the mean of the logits of the training dataset to the test Tensor, then use the KNN to classify the most possible breed was the most convenient choice. Through this method, it reached accuracy and f1-score of `68.80%`, `68.82%`, respectively.

The system normally alerts when it is indicated, through the `breed input text` on predict form, a non-registered bread. But in the case of the top 3 breeds that have their confidence standard deviation less than `0.1%` (this value was reached by trial and error through a human inspection) the system will indicate the probable breed as *Unknown*.

**Notebooks:**

- `data_preparation.ipynb`: Creates the `train.csv` file (image path, dog detection box, and label) for use only images with dogs in them (we found some bad images on the original dataset);
- `training_eval_model.ipynb`: Train and eval the NeuralNetwork developed to detect the 100 breeds. The network reached `70.37%`, `70.89%` of accuracy and f1-score, respectively;
- `distance_classifier_test.ipynb`: Uses the one against feature means distance computation approach;
- `distance_classifier_second_test.ipynb`: Uses the one against all feature distance computation approach;

## App Folder

The system app was developed using as core the stack: FastAPI and PyTorch. The code is strucutred as follow:

```sh
- app               >> root
  - alembic         >> alembic database migration scripts
  - api             >> REST api routes
  - core            >> app core structure (config and startup/shutdown handlers)
  - db              >> database structure (access, crud, and schema models)
  - models          >> pydantic models
  - pages           >> html endpoints
  - services        >> app services to detect dogs, recognize breeds, and enroll breeds
  - logger.py       >> app logger
  - main.py         >> app main
- data              >> system data (features and initial breeds)
- static            >> static assets
- templates         >> html templates
```

The system unit tests are available on the `tests` folder and uses some `samples` images for basic system funcionality test.
