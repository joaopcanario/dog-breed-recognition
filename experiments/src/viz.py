# import functools

import numpy as np
import pandas as pd

from tabulate import tabulate

# from src.dogs import dataset


def metrics(accs, fscores):
    assert len(accs) == len(fscores), \
        'Number of folds must be equal in accurecies and f1-scores'

    rows = [f'Fold {i}' for i in range(1, len(accs) + 1)]
    accs = np.asarray(accs)
    fscores = np.asarray(fscores)

    acc_values = [f'{a * 100:.2f}%' for a in accs.tolist()]
    fscore_values = [f'{a * 100:.2f}%' for a in fscores.tolist()]

    data = {k: [a, f] for k, a, f in zip(rows, acc_values, fscore_values)}
    df = pd.DataFrame(data).T

    print("\n", tabulate(df, headers=['Accuracy', 'F1-Score']), "\n")

    rows = ["Mean", "Std"]
    acc_values = [fscores.mean(), fscores.std()]
    fscore_values = [accs.mean(), accs.std()]

    data = {k: [a, f] for k, a, f in zip(rows, acc_values, fscore_values)}
    df = pd.DataFrame(data).T

    print("\n-------  ----------  ----------\n", tabulate(df), "\n")


# def confusion_matrix(cmatrixes):
#     classes = dataset.classes()
#     cmatrix = functools.reduce(np.add, cmatrixes)

#     data = {k: row.tolist() for k, row in zip(classes, cmatrix)}
#     df = pd.DataFrame(data).T

#     print("\n", tabulate(df, headers=classes), "\n")
