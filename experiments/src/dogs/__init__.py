import cv2
import torch
import torchvision

import numpy as np

from PIL import Image

from . import dataset


def search(impath, yolo):
    dtype = "cuda" if torch.cuda.is_available() else "cpu"
    device = torch.device(dtype)

    im = cv2.imread(impath)
    orig = im.copy()
    
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

    yolo.eval()
    yolo.to(device)

    with torch.no_grad():
        result = yolo(im)

    result = result.pandas().xyxy[0]

    dogs = result[result['name'] == 'dog']

    box = tuple(result.iloc[0, 0:4]) if not dogs.empty \
        else (0, 0, orig.shape[1], orig.shape[0])

    return orig, box


def breed(im, model, k=5):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    transform = torchvision.transforms.Compose([
        torchvision.transforms.Resize(256),
        torchvision.transforms.CenterCrop(224),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225]),
    ])

    image = Image.fromarray(im)

    batch = transform(image).unsqueeze(0)
    batch = batch.to(device)

    outputs = model(batch)

    p = torch.topk(torch.nn.functional.softmax(outputs, dim=1), k)

    confidences = p.values.squeeze().tolist()
    confidences = map(lambda v: f"{v * 100:.2f}%", confidences)

    indices = p.indices.squeeze().tolist()
    indices = map(int, indices)

    predictions = [(c, dataset.decode_label(i))
                   for i, c in zip(indices, confidences)]

    return "\n".join(predictions)
