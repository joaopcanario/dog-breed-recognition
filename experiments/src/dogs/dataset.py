import os
import random
import torch
import torchvision

import numpy as np
import pandas as pd

from PIL import Image

from sklearn.preprocessing import MultiLabelBinarizer as MLB
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold as SKFold
from torch.utils.data import DataLoader
from torch.utils.data import Dataset


class DogDataset(Dataset):
    def __init__(self, dataset, transform):
        self.root = os.getcwd()
        self.paths, self.targets = dataset
        self.transform = transform

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, idx):
        path, box = self.paths[idx]

        im = Image.open(f'{self.root}/{path}').convert('RGB').crop(box)

        sample = self.transform(im)
        target = torch.tensor(self.targets[idx]).argmax(dim=-1)

        return sample, target


def preprocess(X, y, transform):
    root = os.getcwd()

    data = [transform(Image.open(f'{root}/{path}').convert('RGB').crop(box))
            for path, box in X]

    X = torch.stack(data)
    y = torch.tensor(y).argmax(dim=-1)

    return X, y


def folds(datapath, n_splits, test_size=.3,
          batch_size=16, shuffle=False, seed=None):
    if seed:
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.backends.cudnn.benchmark = False
        torch.use_deterministic_algorithms(False)

    transform = torchvision.transforms.Compose([
        torchvision.transforms.Resize(256),
        torchvision.transforms.CenterCrop(224),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225]),
    ])

    kfold = SKFold(n_splits=n_splits, shuffle=shuffle, random_state=seed)
    workers = os.cpu_count()

    print("Load Dataset")
    (X, y), (X_test, y_test) = load_and_split(datapath, seed, test_size)

    print("Preprocess Test Dataset")
    testset = preprocess(X_test, y_test, transform)

    print(f"K-Fold Cross Validation (n_splits={n_splits})")
    for i, (train, val) in enumerate(kfold.split(X, np.argmax(y, axis=1)), 1):
        print(f"Fold: {i}")
        trainloader = DataLoader(DogDataset((X[train], y[train]), transform),
                                 batch_size=batch_size, num_workers=workers,
                                 shuffle=True)

        valloader = DataLoader(DogDataset((X[val], y[val]), transform),
                               batch_size=batch_size, num_workers=workers,
                               shuffle=False)

        yield trainloader, valloader, testset


def load(filename="data/train.csv", dtype=None):
    if not dtype:
        dtype = {'path': str, 'x': int, 'y': int,
                 'w': int, 'h': int, 'label': str}

    df = pd.read_csv(filename, header=0, dtype=dtype, engine='c')

    X = np.asarray([[row.path, (row.x, row.y, row.w, row.h)]
                    for row in df.itertuples(index=False)])

    y = MLB().fit_transform([[l] for l in df['label'].tolist()])

    return X, y


def classes(data_path="data/train.csv"):
    df = pd.read_csv(data_path, header=0, engine='c')

    labels = list(df['label'].unique())
    labels.sort()

    return labels


def decode_label(prediction, data_path="data/train.csv"):
    labels = classes(data_path)
    return "Unknown" if prediction == len(labels) else labels[prediction]


def load_and_split(datapath, seed, test_size=0.3):
    X, y = load(datapath)
    return split(X, y, seed, test_size)


def split(X, y, seed, test_size=0.3):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=test_size, random_state=seed, stratify=y)

    return (X_train, y_train), (X_test, y_test)
