import torch
import torchmetrics
import torchvision

import pytorch_lightning as pl


class CanisBreedNet(pl.LightningModule):
    def __init__(self, output_size):
        super().__init__()

        m = torchvision.models.densenet201(pretrained=True)

        self.features = m.features

        for param in self.parameters():
            param.requires_grad = False

        self.classifier = torch.nn.LazyLinear(output_size, bias=True)

    def forward(self, X, **kwargs):
        X = self.features(X)
        X = torch.flatten(X, start_dim=1)
        X = self.classifier(X)

        return X

    def configure_optimizers(self):
        return torch.optim.Adamax(self.parameters(), lr=1e-3, eps=1e-07)

    def training_step(self, batch, batch_idx):
        x, y = batch
        logits = self(x)

        loss = torch.nn.functional.cross_entropy(logits, y)
        acc = torchmetrics.functional.accuracy(logits, y)

        self.log('train_loss', loss)
        self.log('train_acc', acc, on_step=False, on_epoch=True, prog_bar=True)

        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        logits = self(x)

        loss = torch.nn.functional.cross_entropy(logits, y)
        acc = torchmetrics.functional.accuracy(logits, y)

        self.log('val_loss', loss)
        self.log('valid_acc', acc, on_step=False, on_epoch=True, prog_bar=True)
