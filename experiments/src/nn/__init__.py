import torch
import sys

import numpy as np
import pytorch_lightning as pl
import sklearn.metrics as metrics

from pathlib import Path
from tqdm import tqdm

from .. import viz
from ..dogs import dataset
from ..nn import models


class LitProgressBar(pl.callbacks.progress.ProgressBar):
    def init_sanity_tqdm(self) -> tqdm:
        """Override this to customize the tqdm bar for the validation sanity run."""
        bar = tqdm(desc="Validation sanity check",
                   position=(2 * self.process_position), disable=self.is_disabled,
                   leave=False, dynamic_ncols=True, file=sys.stdout)

        return bar

    def init_train_tqdm(self) -> tqdm:
        """Override this to customize the tqdm bar for training."""
        bar = tqdm(desc="Training", initial=self.train_batch_idx, position=0,
                   disable=self.is_disabled, leave=True, dynamic_ncols=True,
                   file=sys.stdout, smoothing=0)

        return bar

    def init_predict_tqdm(self) -> tqdm:
        """Override this to customize the tqdm bar for predicting."""
        bar = tqdm(desc="Predicting", initial=self.train_batch_idx, position=0,
                   disable=self.is_disabled, leave=True, dynamic_ncols=True,
                   file=sys.stdout, smoothing=0)

        return bar

    def init_validation_tqdm(self) -> tqdm:
        """Override this to customize the tqdm bar for validation."""
        bar = tqdm(desc="Validating", position=0, disable=self.is_disabled,
                   leave=False, dynamic_ncols=True, file=sys.stdout)

        return bar

    def init_test_tqdm(self) -> tqdm:
        """Override this to customize the tqdm bar for testing."""
        bar = tqdm(desc="Testing", position=0, disable=self.is_disabled,
                   leave=True, dynamic_ncols=True, file=sys.stdout)

        return bar


def train(datapath, batch_size=16, seed=None, folds=5,
          test_size=.3, save=False, n_classes=100, kwargs={}):
    if seed:
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.backends.cudnn.benchmark = False
        torch.set_deterministic(False)

    if torch.cuda.is_available():
        kwargs['gpus'] = 1
        kwargs['accelerator'] = 'dp'
    else:
        kwargs['gpus'] = 0

    accs = []
    fscores = []
    cmatrixes = []

    models_path = Path("data/models")

    fold = 1

    for trainloader, valloader, testdata in dataset.\
        folds(datapath, folds, test_size=test_size,
              batch_size=batch_size, shuffle=True, seed=seed):

        net = models.CanisBreedNet(n_classes)

        trainer = pl.Trainer(**kwargs)
        trainer.fit(net, trainloader, valloader)
        
        print("Testing model")

        X_test, y_test = testdata
        y_pred = np.argmax(net(X_test).detach().numpy(), axis=1)

        acc = metrics.accuracy_score(y_test.numpy(), y_pred)
        fscore = metrics.f1_score(y_test.numpy(), y_pred, average=None).mean()
        # cm = metrics.confusion_matrix(y_test.numpy(), y_pred)
        
        print(f"Accuracy: {acc * 100:.2f}%")
        print(f"F1-score: {fscore * 100:.2f}%")

        accs.append(acc)
        fscores.append(fscore)
        # cmatrixes.append(cm)

        if save:
            file = str(models_path / f'Breed_{fold}.pth')
            torch.save(net.state_dict(), file)

        fold += 1

    viz.metrics(accs, fscores)

    if save:
        best_fold = accs.index(max(accs))
        best_model = models_path / f'Breed_{best_fold + 1}.pth'
        best_model.rename(models_path / f'CanisBreedNet.pth')

        for i in range(1, folds + 1):
            model_file = models_path / f'Breed_{i}.pth'
            model_file.unlink(missing_ok=True)
