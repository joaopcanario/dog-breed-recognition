// Predict process

const choosePredict = document.getElementById("predict-btn");
const predictForm = document.getElementById("predict-breed");

choosePredict.addEventListener("click", function () {
  document.getElementById("predict").click();
});

predictForm.addEventListener("submit", (event) => {
  const modal = document.getElementById("lock-modal");
  const loading = document.getElementById("loading");

  // stop form submission
  event.preventDefault();

  // lock down the form
  modal.style.visibility = "visible";
  loading.style.visibility = "visible";

  const formData = new FormData(predictForm);

  axios
    .post("/api/v1/breed/predict", formData, {
      "Content-Type": "multipart/form-data",
    })
    .then((response) => showPrediction(response))
    .catch((error) => {
      alert(error);
      console.log(error);
    })
    .then(() => {
      modal.style.visibility = "hidden";
      loading.style.visibility = "hidden";
    });
});

// Enroll process

const chooseEnroll = document.getElementById("enroll-btn");
const enrollForm = document.getElementById("enroll-breed");

chooseEnroll.addEventListener("click", function () {
  document.getElementById("enroll").click();
});

enrollForm.addEventListener("submit", (event) => {
  const modal = document.getElementById("lock-modal");
  const loading = document.getElementById("loading");

  // stop form submission
  event.preventDefault();

  // lock down the form
  modal.style.visibility = "visible";
  loading.style.visibility = "visible";

  const formData = new FormData(enrollForm);

  axios
    .post("/api/v1/breed/enroll", formData, {
      "Content-Type": "multipart/form-data",
    })
    .then((response) => showEnroll(response))
    .catch((error) => {
      alert(error);
      console.log(error);
    })
    .then(() => {
      modal.style.visibility = "hidden";
      loading.style.visibility = "hidden";
    });
});

function showEnroll(response) {
  const content = document.getElementById("content");
  content.innerHTML = `<p>Breed <strong>registered</strong> successfully!</p>`;
}

function showPrediction(response) {
  const content = document.getElementById("content");

  content.innerHTML = `<p>Correct Breed: <strong>${response.data.expected}</strong></p>`;
  content.innerHTML += `<p>Breed with Highest Confidence:: <strong>${response.data.pbreed}</strong></p>`;
  content.innerHTML += `<p>Top 5 breeds:</p>`;

  // creates a <table> element and a <tbody> element
  const tbl = document.createElement("table");
  const tblHead = document.createElement("thead");

  tblHead.innerHTML = `<tr><th>Breed</th><th>Confidence</th></tr>`;
  tbl.appendChild(tblHead);

  const tblBody = document.createElement("tbody");

  response.data.topk.forEach(function (el) {
    let row = document.createElement("tr");
    let cellBreed = document.createElement("td");
    let cellConfidence = document.createElement("td");

    cellBreed.appendChild(document.createTextNode(`${el.breed}`));
    cellConfidence.appendChild(
      document.createTextNode(`${el.confidence.toFixed(4)}`)
    );

    row.appendChild(cellBreed);
    row.appendChild(cellConfidence);

    tblBody.appendChild(row);
  });

  tbl.appendChild(tblBody);

  content.appendChild(document.createElement("p").appendChild(tbl));
}
