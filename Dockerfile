FROM nvcr.io/nvidia/pytorch:21.07-py3

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /code

COPY pyproject.toml ./
COPY alembic.ini ./
COPY data/ ./data/
COPY app/ ./app/
COPY static/ ./static/
COPY templates/ ./templates

RUN pip install --upgrade pip && \
    pip install --no-cache-dir wheel && \
    pip install --no-cache-dir 'poetry==1.1.7' && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction --no-ansi

COPY entrypoint.sh ./

ENTRYPOINT ["/code/entrypoint.sh"]
