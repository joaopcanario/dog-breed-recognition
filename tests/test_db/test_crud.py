import io
import torch

from sqlalchemy.orm.session import Session

from app.db import crud
from app.db.models import Breed


def test_count_breeds(session: Session) -> None:
    count = crud.count_breeds(session)

    assert count == 7


def test_get_breed(session: Session) -> None:
    breed = crud.get_breed_by_id(session, 1)
    expected = Breed(**{"id": 1, "name": "Affenpinscher"})

    assert breed.id == expected.id
    assert breed.name == expected.name


def test_get_breed_by_name(session: Session) -> None:
    breed = crud.get_breed_by_name(session, "Affenpinscher")
    expected = Breed(**{"id": 1, "name": "Affenpinscher"})

    assert breed.id == expected.id
    assert breed.name == expected.name


def test_get_breeds_by_ids(session: Session) -> None:
    breeds = crud.get_breeds_by_ids(session, [1, 2, 3])
    breeds = [(r.id, r.name) for r in breeds]

    expected = [(1, "Affenpinscher"), (2, "Afghan Hound"), (3, "Airedale")]

    assert breeds == expected


def test_get_breeds(session: Session) -> None:
    breeds = crud.get_breeds(session)

    assert len(breeds) == 7


def test_count_features(session: Session) -> None:
    count = crud.count_features(session)

    assert count == 7


def test_get_feature_by_breed_id(session: Session) -> None:
    feature = crud.get_feature_by_breed_id(session, 3)

    assert feature.breed_id == 3


def test_get_features(session: Session) -> None:
    features = crud.get_features(session)

    assert len(features) == 7
