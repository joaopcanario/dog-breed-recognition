def test_heartbeat(client) -> None:
    response = client.get('api/v1/health/')

    assert response.status_code == 200
    assert response.json() == {"is_alive": True}
