from pathlib import Path

from app.core import config


def test_breed_predict(client) -> None:
    filename = Path(f"{config.ROOT}/samples/predict/test.jpg")
    data = {'image': filename.open(mode='rb')}
    payload = {"breed": "Airedale"}

    response = client.post("api/v1/breed/predict", files=data, data=payload)

    expected = [
        {'breed': 'Airedale', 'confidence': 0.28046974539756775},
        {'breed': 'Wire-Haired Fox Terrier', 'confidence': 0.227540984749794},
        {'breed': 'Afghan Hound', 'confidence': 0.16561691462993622},
        {'breed': 'Chesapeake Bay Retriever', 'confidence': 0.16496619582176208},
        {'breed': 'Affenpinscher', 'confidence': 0.16140620410442352},
    ]

    assert response.status_code == 200
    assert response.json()['pbreed'] == "Airedale"
    assert response.json()['topk'] == expected


def test_breed_enroll(client) -> None:
    enroll = Path(f"{config.ROOT}/samples/enroll-train/")

    data = [('images', im.open(mode='rb')) for im in enroll.glob("*.jpg")]
    payload = {"breed": "Rottweiler"}

    response = client.post("/api/v1/breed/enroll", files=data, data=payload)

    assert response.status_code == 200
    assert response.json() == {'name': 'Rottweiler'}
