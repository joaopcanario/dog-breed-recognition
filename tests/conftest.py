import pytest

import sqlalchemy as sa

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session

from app.core import config
from app.db.models import Base, Breed, Feature
from app.db.database import session_db

from app.core import config
from app.main import app  # noqa: E402


engine = create_engine(
    config.DATABASE_URL, connect_args={"check_same_thread": False}
)

TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine)

# Set up the database once
Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind=engine)


def seed_database(db: Session):
    labels = [{'name': 'Affenpinscher'},
              {'name': 'Afghan Hound'},
              {'name': 'Airedale'},
              {'name': 'Brabancon Griffon'},
              {'name': 'Chesapeake Bay Retriever'},
              {'name': 'Welsh Springer Spaniel'},
              {'name': 'Wire-Haired Fox Terrier'}]

    db.bulk_insert_mappings(Breed, labels)

    data = [{'breed_id': i} for i in [1, 2, 3, 20, 25, 97, 99]]

    db.bulk_insert_mappings(Feature, data)
    db.commit()


# These two event listeners are only needed for sqlite for proper
# SAVEPOINT / nested transaction support. Other databases like postgres
# don't need them.
# From: https://docs.sqlalchemy.org/en/14/dialects/sqlite.html#serializable-isolation-savepoints-transactional-ddl
@sa.event.listens_for(engine, "connect")
def do_connect(dbapi_connection, connection_record):
    # disable pysqlite's emitting of the BEGIN statement entirely.
    # also stops it from emitting COMMIT before any DDL.
    dbapi_connection.isolation_level = None


@sa.event.listens_for(engine, "begin")
def do_begin(conn):
    # emit our own BEGIN
    conn.exec_driver_sql("BEGIN")


# This fixture creates a nested transaction, recreates it when the
# applications code calls session.commit and rolls it back at the end.
# Based on: https://docs.sqlalchemy.org/en/14/orm/session_transaction.html#joining-a-session-into-an-external-transaction-such-as-for-test-suites
@pytest.fixture()
def session():
    connection = engine.connect()
    transaction = connection.begin()
    session = TestingSessionLocal(bind=connection)

    # Begin a nested transaction (using SAVEPOINT).
    nested = connection.begin_nested()

    # If the application code calls session.commit, it will end the nested
    # transaction. Need to start a new one when that happens.
    @sa.event.listens_for(session, "after_transaction_end")
    def end_savepoint(session, transaction):
        nonlocal nested
        if not nested.is_active:
            nested = connection.begin_nested()

    seed_database(session)
    yield session

    # Rollback the overall transaction, restoring the state before the test ran.
    session.close()
    transaction.rollback()
    connection.close()


# A fixture for the fastapi test client which depends on the
# previous session fixture. Instead of creating a new session in the
# dependency override as before, it uses the one provided by the
# session fixture.
@pytest.fixture()
def client(session):
    def override_get_db():
        yield session

    app.state.db = session
    app.dependency_overrides[session_db] = override_get_db

    with TestClient(app) as client:
        yield client

    del app.dependency_overrides[session_db]
