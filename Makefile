.PHONY : help clean dropdb install migrations run

help:
	@echo ""
	@echo "Dog Breed Recognition"
	@echo ""
	@echo ""
	@echo "Available commands:"
	@echo "    clear \t Clean deployment assets and remove created database"
	@echo "    docker \t Run project through docker containers"
	@echo "    install \t Install project dependencies"
	@echo "    migrations \t Run database migrations"
	@echo "    rundev \t Run locally"
	@echo "    testapp \t Run unit tests"

clear:
	@echo ""
	@echo "Cleaning generated files during deployment"
	@rm -rf deploy
	@rm -rf fetch
	@echo "Drop Database"
	@rm -f breed.db

docker:
	@echo ""
	@echo "Building and runing docker container"
	@echo ""
	@docker-compose up

install:
	@echo ""
	@echo "Install dependencies"
	@echo ""
	@poetry install --no-root --remove-untracked

migrations:
	@echo ""
	@echo "Runing Database Migrations"
	@echo ""
	@poetry run alembic upgrade head

rundev: install migrations
	@echo ""
	@echo "Runing Application Locally"
	@echo ""
	@poetry run uvicorn app.main:app --host 0.0.0.0 --port 6500 --log-level info --reload

testapp:
	@echo ""
	@echo "Runing Application Unit Test"
	@echo ""
	@poetry run pytest tests -vv --disable-warnings
