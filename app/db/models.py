from sqlalchemy import Column, Integer, ForeignKey, String, LargeBinary
from sqlalchemy.orm import relationship

from .database import Base


class Breed(Base):
    __tablename__ = "breeds"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True, nullable=False)

    features = relationship("Feature", back_populates="breed")


class Feature(Base):
    __tablename__ = "features"

    id = Column(Integer, primary_key=True, index=True)

    breed_id = Column(Integer, ForeignKey("breeds.id"))

    breed = relationship("Breed", back_populates="features")
