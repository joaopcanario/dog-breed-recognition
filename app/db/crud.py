from sqlalchemy.orm import Session
from typing import List

from app.models.breed import Breed, Feature

from . import models


def count_breeds(db: Session) -> int:
    return db.query(models.Breed).count()


def get_breed_by_id(db: Session, breed_id: int) -> models.Breed:
    return db.query(models.Breed).filter(models.Breed.id == breed_id).first()


def get_breed_by_name(db: Session, breed_name: int) -> models.Breed:
    return db.query(models.Breed).filter(models.Breed.name == breed_name).first()


def get_breeds_by_ids(db: Session, ids: List[int]) -> List[models.Breed]:
    return db.query(models.Breed).filter(models.Breed.id.in_(ids)).all()


def get_breeds(db: Session, skip: int = 0, limit: int = 100) -> List[models.Breed]:
    return db.query(models.Breed).offset(skip).limit(limit).all()


def create_breed(db: Session, breed: Breed) -> models.Breed:
    db_breed = models.Breed(**breed.dict())

    db.add(db_breed)
    db.commit()
    db.refresh(db_breed)

    return db_breed


def count_features(db: Session) -> int:
    return db.query(models.Feature).count()


def get_feature_by_breed_id(db: Session, breed_id: int) -> models.Feature:
    return db.query(models.Feature).filter(models.Feature.breed_id == breed_id).first()


def get_features(db: Session) -> List[models.Feature]:
    return db.query(models.Feature).with_entities(models.Feature.breed_id).all()


def create_feature(db: Session, feature: Feature) -> models.Feature:
    db_breed = models.Feature(**feature.dict())

    db.add(db_breed)
    db.commit()
    db.refresh(db_breed)

    return db_breed
