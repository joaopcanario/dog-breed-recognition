from fastapi import APIRouter

from app.models.heartbeat import HearbeatResult

router = APIRouter()


@router.get("/", response_model=HearbeatResult, name="heartbeat")
async def get_hearbeat() -> HearbeatResult:
    return HearbeatResult(is_alive=True)
