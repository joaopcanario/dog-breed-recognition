import gc
import time
import torch

from fastapi import BackgroundTasks, Depends, HTTPException
from fastapi import APIRouter, File, UploadFile, Form
from pathlib import Path
from sqlalchemy.orm import Session
from starlette.requests import Request
from typing import List

from app.core import config as cfg
from app.db import crud, database
from app.logger import logger
from app.models.image import Image
from app.models.breed import BreedCreate, FeatureCreate
from app.models.prediction import Prediction
from app.services.breeds import recognize
from app.services.dogs import detect
from app.services.models import update


router = APIRouter()


def enroll_features(images, yolo, model, breed_id, session):
    gc.collect()
    torch.cuda.empty_cache()

    part_time = start_time = time.time()

    images = [Image(**{"content": i.file.read()}).content for i in images]

    logger.info((f"Load content took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.tsime()

    X = [detect(x, yolo) for x in images]

    logger.info((f"Dog detection on the {len(images)} images took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    X = update(X, model)

    logger.info((f"Features extraction on the {len(images)} images took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    if not Path(f'{cfg.ROOT}/data/X_{breed_id}.pt').exists():
        logger.info("Features already stored on disk")
        torch.save(X, f'{cfg.ROOT}/data/X_{breed_id}.pt')

    data = FeatureCreate(**{'breed_id': breed_id})
    db_feature = crud.create_feature(session, feature=data)

    logger.info((f"Enroll features ({db_feature.id}) on DB took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    logger.info((f"Enroll process took "
                 f"{(time.time() - start_time):.3f} seconds"))


@router.post("/enroll", response_model=BreedCreate, name="enroll")
async def enroll(
    request: Request,
    background_tasks: BackgroundTasks,
    breed: str = Form(...),
    images: List[UploadFile] = File(...),
    db: Session = Depends(database.session_db)
) -> BreedCreate:
    gc.collect()
    torch.cuda.empty_cache()

    part_time = start_time = time.time()

    db_breed = crud.get_breed_by_name(db, breed)

    logger.info(("Verify if breed is registered took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    if db_breed:
        raise HTTPException(status_code=400, detail="Breed Already Registered")

    db_breed = crud.create_breed(db=db, breed=BreedCreate(**{"name": breed}))

    logger.info((f"Bread creation ({db_breed.name}) on DB took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    background_tasks.add_task(enroll_features,
                              images,
                              request.app.state.yolo,
                              request.app.state.model,
                              db_breed.id, db)

    logger.info(("Enroll endpoint execution time: "
                 f"{(time.time() - start_time):.3f} seconds"))

    return BreedCreate(**{"name": db_breed.name})


@router.post("/predict", response_model=Prediction, name="predict")
async def predict(
    request: Request,
    breed: str = Form(...),
    image: UploadFile = File(...),
    db: Session = Depends(database.session_db)
) -> Prediction:
    gc.collect()
    torch.cuda.empty_cache()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    part_time = start_time = time.time()

    db_breed = crud.get_breed_by_name(db, breed)

    logger.info(("Verify if breed is registered took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    if not db_breed:
        db_breed = crud.get_breeds(db)
        available_breeds = ", ".join([b.name for b in db_breed])

        detail = " ".join(["Breed Not Registered. Available breeds:",
                           available_breeds])

        logger.info(f"Not found: {detail}")

        raise HTTPException(status_code=404, detail=detail)

    im = detect(Image(**{"content": image.file.read()}).content,
                request.app.state.yolo)

    logger.info(("Dog detection took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    db_features = crud.get_features(db)
    db_features = [f'{cfg.ROOT}/data/X_{r.breed_id}.pt' for r in db_features]

    features = torch.stack([torch.load(p).to(device) for p in db_features])
    features.to(device)

    logger.info(("Load features of breeds from DB took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    ids, confidences, top3std = \
        recognize(im, request.app.state.model, features)

    logger.info(("Breed recognition took "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    breeds = crud.get_breeds_by_ids(db, ids)
    breeds = [b.name for i in ids for b in breeds if b.id == i]

    pbreed = "Unknown" if bool(top3std < 1e-3) else breeds[0]
    topk = [{"breed": b, "confidence": c} for b, c in zip(breeds, confidences)]

    logger.info(("Prepare response in "
                 f"{(time.time() - part_time):.3f} seconds"))
    part_time = time.time()

    logger.info(("Predict endpoint execution time: "
                 f"{(time.time() - start_time):.3f} seconds"))

    payload = {"expected": breed, "topk": topk, "pbreed": pbreed}
    return Prediction(**payload)
