from fastapi import APIRouter

from .routes import breed
from .routes import heartbeat


def api_routes():
    router = APIRouter()

    router.include_router(heartbeat.router, tags=["health"], prefix="/health")
    router.include_router(breed.router, tags=["breed"], prefix="/breed")

    return router
