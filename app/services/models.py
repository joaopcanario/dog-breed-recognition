import torch
import torchvision


def update(X, model):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    transform = torchvision.transforms.Compose([
        torchvision.transforms.Resize(256),
        torchvision.transforms.CenterCrop(224),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225]),
    ])

    model.eval()
    model.to(device)

    X = torch.stack([transform(x) for x in X])
    X = X.to(device)

    with torch.no_grad():
        X = model(X).detach()

    X = X.mean(dim=0)

    return X
