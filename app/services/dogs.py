import cv2
import torch

import numpy as np

from PIL import Image


def detect(im, yolo):
    dtype = "cuda" if torch.cuda.is_available() else "cpu"
    device = torch.device(dtype)

    orig = im.copy()
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

    yolo.eval()
    yolo.to(device)

    with torch.no_grad():
        result = yolo(im)

    result = result.pandas().xyxy[0]

    dogs = result[result['name'] == 'dog']

    box = tuple(result.iloc[0, 0:4]) if not dogs.empty \
        else (0, 0, orig.shape[1], orig.shape[0])

    data = Image.fromarray(orig).convert('RGB').crop(box)

    return data
