import torch
import torchvision

import numpy as np

from PIL import Image


def recognize(im, model, db_features, k=5):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    transform = torchvision.transforms.Compose([
        torchvision.transforms.Resize(256),
        torchvision.transforms.CenterCrop(224),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225]),
    ])

    im = Image.fromarray(im) if isinstance(im, np.ndarray) else im

    model.eval()
    model.to(device)

    input_data = transform(im).unsqueeze(0)
    input_data = input_data.to(device)

    with torch.no_grad():
        outputs = model(input_data).detach()

    dist = torch.cdist(db_features.to(device), outputs.unsqueeze(0)).squeeze()
    values, indexes = dist.topk(k, largest=False)

    confidences = 1 / values.squeeze()
    confidences = (confidences / confidences.sum())
    top3std = torch.std(confidences[:3])

    indices = list(map(lambda i: int(i) + 1, indexes.squeeze().tolist()))

    return indices, confidences.tolist(), top3std
