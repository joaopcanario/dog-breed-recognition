import pydantic

from typing import List


class FeatureCreate(pydantic.BaseModel):
    breed_id: int


class Feature(pydantic.BaseModel):
    id: int
    breed_id: int

    class Config:
        orm_mode = True


class BreedBase(pydantic.BaseModel):
    name: str


class BreedCreate(BreedBase):
    pass


class Breed(BreedBase):
    id: int
    features: List[Feature] = []

    class Config:
        orm_mode = True
