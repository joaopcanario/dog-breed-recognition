import cv2
import pydantic
import numpy as np

class Image(pydantic.BaseModel):
    content: np.ndarray

    class Config:
        arbitrary_types_allowed = True

    @pydantic.validator("content", pre=True)
    def pre_process_image(cls, v):
        data = np.frombuffer(v, np.uint8)

        im = cv2.imdecode(data, cv2.IMREAD_GRAYSCALE)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        im = clahe.apply(im)
        im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)

        return im
