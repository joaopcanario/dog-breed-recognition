import pydantic

from typing import List


class Predict(pydantic.BaseModel):
    breed: str
    confidence: float


class Prediction(pydantic.BaseModel):
    expected: str
    pbreed: str
    topk: List[Predict]
