import os

APP_VERSION = "1.0.0"
APP_NAME = "Canis Breed"
API_PREFIX = "/api/v1"

ROOT = os.getenv("WORKDIR", ".")

DATABASE_URL = os.getenv("DATABASE_URL", "sqlite:///./breed.db")
