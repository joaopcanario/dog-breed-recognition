import torch

from fastapi import FastAPI
from torchvision.models import densenet201
from typing import Callable

from app.logger import logger


def startup_event(app: FastAPI) -> Callable:
    def startup() -> None:
        logger.info("Running app start handler.")

        app.state.model = densenet201(pretrained=True)
        app.state.yolo = torch.hub.load('ultralytics/yolov5', 'yolov5s',
                                        pretrained=True)

    return startup


def shutdown_event(app: FastAPI) -> Callable:
    def shutdown() -> None:
        logger.info("Running app shutdown handler.")

        app.state.model = None

    return shutdown
