"""create breed table

Revision ID: 086523752974
Revises:
Create Date: 2021-08-05 14:35:55.944364

"""
import os
import csv

import sqlalchemy as sa

from alembic import op

from app.core import config


# revision identifiers, used by Alembic.
revision = '086523752974'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():

    conn = op.get_bind()
    inspector = sa.engine.reflection.Inspector.from_engine(conn)
    tables = inspector.get_table_names()

    if 'breeds' not in tables:
        breeds_table = op.create_table(
            'breeds',
            sa.Column('id', sa.Integer, primary_key=True, index=True),
            sa.Column('name', sa.String(50), unique=True,
                      index=True, nullable=False),
        )

        filename = f"{config.ROOT}/data/breeds.csv"

        with open(str(filename), mode='r') as f:
            csv_reader = csv.DictReader(f)
            breeds = [{'name': row['breed']} for row in csv_reader]

        op.bulk_insert(breeds_table, breeds)


def downgrade():
    op.drop_table('breeds')
