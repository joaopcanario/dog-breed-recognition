"""create feature table and add relationship between tables

Revision ID: 42d1f1fa57c4
Revises: 086523752974
Create Date: 2021-08-08 00:55:27.363360

"""
import re

import sqlalchemy as sa

from alembic import op
from pathlib import Path

from app.core import config


# revision identifiers, used by Alembic.
revision = '42d1f1fa57c4'
down_revision = '086523752974'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    inspector = sa.engine.reflection.Inspector.from_engine(conn)
    tables = inspector.get_table_names()

    if 'features' not in tables:
        features_table = op.create_table(
            'features',
            sa.Column('id', sa.Integer, primary_key=True, index=True),

            sa.Column('breed_id', sa.Integer, sa.ForeignKey("breeds.id")),
        )

    data_path = Path(f"{config.ROOT}/data/")

    data = [{'breed_id': int(re.findall(r'\d+', fname.name)[0])}
            for fname in data_path.glob("*.pt")]

    op.bulk_insert(features_table, data)


def downgrade():
    pass
