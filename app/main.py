import warnings

from fastapi import FastAPI, Response
from fastapi.staticfiles import StaticFiles
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request

from app.api.router import api_routes
from app.db import database
from app.core.handlers import startup_event, shutdown_event
from app.core.config import APP_NAME, API_PREFIX, APP_VERSION
from app.logger import logger
from app.pages import root


warnings.filterwarnings('ignore')


def get_app() -> FastAPI:
    fast_app = FastAPI(title=APP_NAME, version=APP_VERSION)

    fast_app.add_middleware(CORSMiddleware,
                            allow_origins=["*"],
                            allow_credentials=True,
                            allow_methods=["*"],
                            allow_headers=["*"])

    fast_app.mount("/static", StaticFiles(directory="static"), name="static")

    fast_app.include_router(root.router)
    fast_app.include_router(api_routes(), prefix=API_PREFIX)

    fast_app.add_event_handler("startup", startup_event(fast_app))
    fast_app.add_event_handler("shutdown", shutdown_event(fast_app))

    logger.info("Create Application")

    return fast_app


app = get_app()


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)

    try:
        request.state.db = database.SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()

    return response
